//
//  Techstore.swift
//  techstore
//
//  Created by Rafael on 04/11/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class Techstore: NSObject {
    private static let BASE_URL = "http://rafaelrsilva.com:3000/api"
    private static let NSUSER_DEFAULT_KEY = "techstore-wishlist"
    
    private static func performRequest(request: URLRequest, completionHandler: ((Data?, URLResponse?, Error?) -> Void)!){
        URLSession.shared.dataTask(with: request, completionHandler: completionHandler).resume()
    }
    
    public static func getCategorias(callback: (([[String: AnyObject]]?) -> Void)!){
        let request: URLRequest = URLRequest(url: URL(string: "\(Techstore.BASE_URL)/categorias")!)
        Techstore.performRequest(request: request, completionHandler: {(data: Data?, response: URLResponse?, error: Error?) -> Void in
            if error == nil && callback != nil {
                callback(try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! [[String: AnyObject]])
            }
        });
    }
    
    public static func getProdutosDaCategoria(categoria: Int, callback: (([[String: AnyObject]]?) -> Void)!){
        let request: URLRequest = URLRequest(url: URL(string: "\(Techstore.BASE_URL)/categorias/\(categoria)/produtos")!)
        Techstore.performRequest(request: request, completionHandler: {(data: Data?, response: URLResponse?, error: Error?) -> Void in
            if error == nil && callback != nil {
                callback(try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! [[String: AnyObject]])
            }
        });
    }
    
    public static func getProduto(produto: Int, callback: (([String: AnyObject]?) -> Void)!){
        let request: URLRequest = URLRequest(url: URL(string: "\(Techstore.BASE_URL)/produtos/\(produto)")!)
        Techstore.performRequest(request: request, completionHandler: {(data: Data?, response: URLResponse?, error: Error?) -> Void in
            if error == nil && callback != nil {
                callback(try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! [String: AnyObject])
            }
        });
    }
    
    public static func getLojas(callback: (([[String: AnyObject]]?) -> Void)!){
        let request: URLRequest = URLRequest(url: URL(string: "\(Techstore.BASE_URL)/lojas")!)
        Techstore.performRequest(request: request, completionHandler: {(data: Data?, response: URLResponse?, error: Error?) -> Void in
            if error == nil && callback != nil {
                callback(try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! [[String: AnyObject]])
            }
        });
    }
    
    public static func getLoja(loja: Int, callback: (([String: AnyObject]?) -> Void)!){
        let request: URLRequest = URLRequest(url: URL(string: "\(Techstore.BASE_URL)/lojas/\(loja)")!)
        Techstore.performRequest(request: request, completionHandler: {(data: Data?, response: URLResponse?, error: Error?) -> Void in
            if error == nil && callback != nil {
                callback(try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as! [String: AnyObject])
            }
        });
    }
    
    private static func getUserDefaultsReference() -> [[String: AnyObject]] {
        let userDefault = UserDefaults.standard.object(forKey: Techstore.NSUSER_DEFAULT_KEY) as? [[String: AnyObject]]
        
        if userDefault != nil {
            return userDefault!
        }
        
        return [[String: AnyObject]]()
    }
    
    private static func setUserDefaults(userDefaults: [[String: AnyObject]]){
        UserDefaults.standard.set(userDefaults, forKey: Techstore.NSUSER_DEFAULT_KEY)
    }
    
    public static func isProdutoNaWishlist(id: Int) -> Int{
        let wishlist = Techstore.getUserDefaultsReference()
        
        if wishlist.count > 0{
            for index in (0 ... wishlist.count - 1){
                if(id == wishlist[index]["id"] as! Int){
                    return index
                }
            }
        }
        
        return -1
    }
    
    public static func getWishlist() -> [[String: AnyObject]]{
        return Techstore.getUserDefaultsReference()
    }
    
    public static func adicionarProdutoNaWishlist(id: Int!, titulo: String, thumb: String) -> Bool{
        if Techstore.isProdutoNaWishlist(id: id) == -1 {
            var wishlist = Techstore.getUserDefaultsReference()
            
            wishlist.append(["id": id as AnyObject, "titulo": titulo as AnyObject, "thumb": thumb as AnyObject]);
            
            Techstore.setUserDefaults(userDefaults: wishlist)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "techStoreWishlistAtualizada"), object: nil)
        }
        
        return true
    }
    
    public static func removerProdutoDaWishlist(id: Int) -> Bool{
        let index = Techstore.isProdutoNaWishlist(id: id)
        
        if index != -1 {
            var wishlist = Techstore.getUserDefaultsReference()
            
            wishlist.remove(at: index)
            
            Techstore.setUserDefaults(userDefaults: wishlist)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "techStoreWishlistAtualizada"), object: nil)
        }
        
        return true
    }
}
