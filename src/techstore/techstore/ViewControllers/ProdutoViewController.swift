//
//  ProdutoViewController.swift
//  techstore
//
//  Created by Rafael on 25/09/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class ProdutoViewController: RRViewController {
    
    var produtoDetalhes: [String: AnyObject]!
    
    @IBOutlet weak var produtoThumb: UIImageView!
    @IBOutlet weak var produtoTitulo: UILabel!
    @IBOutlet weak var produtoDescricao: UITextView!
    @IBOutlet weak var btnToggleWishlist: UIBarButtonItem!
    
    @IBOutlet weak var containerSobre: UIView!
    @IBOutlet weak var containerDetalhes: UIView!
    
    var detalhesTableView: DetalhesTableViewController!
    var wishlist: Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.detalhesTableView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetalhesTableViewController") as! DetalhesTableViewController
        self.containerDetalhes.addSubview(self.detalhesTableView.view)
        self.produtoThumb.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.abrirGaleriaDeFotos)))
        
        Techstore.getProduto(produto: self.produtoDetalhes["id"] as! Int, callback: {(produto: [String : AnyObject]?) -> Void in
            if produto != nil {
                self.produtoDetalhes = produto
                
                DispatchQueue.main.async(execute: {
                    if Techstore.isProdutoNaWishlist(id: self.produtoDetalhes["id"] as! Int) != -1 {
                        self.btnToggleWishlist.image = UIImage(named: "btnWishlistFilled")
                        self.wishlist = true
                    }
                    else{
                        self.btnToggleWishlist.image = UIImage(named: "btnWishlist")
                        self.wishlist = false
                    }
                    
                    self.produtoTitulo.text = self.produtoDetalhes["titulo"] as? String
                    self.produtoDescricao.text = self.produtoDetalhes["descricao"] as? String
                    
                    self.detalhesTableView.arrayData = self.produtoDetalhes["tabela"] as! [[String: AnyObject]]
                    self.detalhesTableView.tableView.reloadData()
                    
                    URLSession.shared.dataTask(with: URL(string: self.produtoDetalhes["thumb"] as! String)!, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
                        DispatchQueue.main.async(execute: {
                            if error == nil && data != nil {
                                self.produtoThumb.image = UIImage(data: data!)
                            }
                            else{
                                self.produtoThumb.image = UIImage(named: "noImages")
                            }
                        })
                    }).resume()
                })
            }
        })
    }
    
    override func viewDidLayoutSubviews() {
        self.detalhesTableView.view.frame = self.containerDetalhes.bounds
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func segmentoAlterado(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.containerSobre.isHidden = false
            self.containerDetalhes.isHidden = true
        }
        else if sender.selectedSegmentIndex == 1 {
            self.containerSobre.isHidden = true
            self.containerDetalhes.isHidden = false
        }
    }
    
    @IBAction func fecharView(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func toggleWishlist(_ sender: Any) {
        if !self.wishlist {
            if Techstore.adicionarProdutoNaWishlist(id: self.produtoDetalhes["id"] as! Int, titulo: self.produtoDetalhes["titulo"] as! String, thumb: self.produtoDetalhes["thumb"] as! String) {
                self.btnToggleWishlist.image = UIImage(named: "btnWishlistFilled")
                self.wishlist = true
            }
        }
        else{
            if Techstore.removerProdutoDaWishlist(id: self.produtoDetalhes["id"] as! Int) {
                self.btnToggleWishlist.image = UIImage(named: "btnWishlist")
                self.wishlist = false
            }
            
        }
    }

    func abrirGaleriaDeFotos(){
    }
}
