//
//  DetalheTableViewCell.swift
//  techstore
//
//  Created by Rafael on 14/11/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class DetalheTableViewCell: UITableViewCell {

    @IBOutlet weak var chave: UILabel!
    @IBOutlet weak var valor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
