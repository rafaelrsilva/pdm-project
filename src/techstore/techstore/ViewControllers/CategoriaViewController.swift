//
//  CategoriaViewController.swift
//  techstore
//
//  Created by Rafael on 25/09/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class CategoriaViewController: RRViewController {

    var categoriaId: Int!
    var categoriaName: String!
    var produtosCollection: ProdutosCollectionViewController!
    
    @IBOutlet weak var header: UINavigationItem!
    @IBOutlet weak var container: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.produtosCollection = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProdutosCollectionViewController") as! ProdutosCollectionViewController
        self.produtosCollection.parentNavigationController = self.navigationController
        self.container.addSubview(self.produtosCollection.view)
        
        header.title = categoriaName
        
        self.updateCategoria(self)
    }
    
    override func viewDidLayoutSubviews() {
        self.produtosCollection.view.frame = self.container.bounds
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func updateCategoria(_ sender: Any) {
        if self.categoriaId != nil {
            Techstore.getProdutosDaCategoria(categoria: self.categoriaId, callback: {(produtos: [[String : AnyObject]]?) in
                DispatchQueue.main.async(execute: {
                    self.produtosCollection.arrayData = produtos!
                    self.produtosCollection.collectionView?.reloadData()
                })
            });
        }
    }
    
    @IBAction func fecharView(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
