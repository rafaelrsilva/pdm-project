//
//  CategoriaTableViewCell.swift
//  techstore
//
//  Created by Rafael on 13/11/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class CategoriaTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var categoriaName: UILabel!
    var categoriaId: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
