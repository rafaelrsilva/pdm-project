//
//  CatalogoViewController.swift
//  techstore
//
//  Created by Rafael on 25/09/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class CatalogoViewController: RRViewController {
    
    @IBOutlet weak var contentContainer: UIView!
    var categoriasListaTableView: CategoriasTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.categoriasListaTableView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriasTableViewController") as! CategoriasTableViewController
        self.categoriasListaTableView.parentNavigationController = self.navigationController
        self.contentContainer.addSubview(categoriasListaTableView.view)
        
        self.updateCatalogo(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLayoutSubviews() {
        self.categoriasListaTableView.view.frame = self.contentContainer.bounds
    }
    
    @IBAction func updateCatalogo(_ sender: Any) {
        Techstore.getCategorias { (categorias: [[String : AnyObject]]? ) in
            DispatchQueue.main.async(execute: {
                self.categoriasListaTableView.arrayData = categorias!
                self.categoriasListaTableView.tableView.reloadData()
            })
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
