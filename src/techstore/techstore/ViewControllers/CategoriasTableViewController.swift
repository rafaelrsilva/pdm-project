//
//  CategoriasTableViewController.swift
//  techstore
//
//  Created by Rafael on 04/11/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class CategoriasTableViewController: UITableViewController {
    
    var parentNavigationController: UINavigationController!
    var arrayData: [[String : AnyObject]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriaTableCell", for: indexPath) as! CategoriaTableViewCell
        let currentIndex = self.arrayData[indexPath.row]
    
        cell.categoriaId = currentIndex["id"] as! Int
        cell.categoriaName.text = currentIndex["titulo"] as? String
        
        URLSession.shared.dataTask(with: URL(string: currentIndex["background"] as! String)!, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            DispatchQueue.main.async(execute: {
                cell.backgroundImage.image = UIImage(data: data!)
            })
        }).resume()

        return cell
    }

    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CategoriaTableViewCell
        let categoriaVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CategoriaViewController") as! CategoriaViewController
        
        categoriaVC.categoriaId = cell.categoriaId
        categoriaVC.categoriaName = cell.categoriaName.text
        
        self.parentNavigationController.pushViewController(categoriaVC, animated: true)
    }
}
