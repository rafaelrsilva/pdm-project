//
//  WishlistViewController.swift
//  techstore
//
//  Created by Rafael on 25/09/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class WishlistViewController: RRViewController {

    @IBOutlet weak var container: UIView!
    var produtosCollection: ProdutosCollectionViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.produtosCollection = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProdutosCollectionViewController") as! ProdutosCollectionViewController
        self.produtosCollection.parentNavigationController = self.navigationController
        self.container.addSubview(self.produtosCollection.view)

        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: "techStoreWishlistAtualizada"), object: nil, queue: OperationQueue.current, using: {(notification: Notification) in
            self.updateWishlist(self)
        })
        
        self.updateWishlist(self)
    }
    
    override func viewDidLayoutSubviews() {
        self.produtosCollection.view.frame = self.container.bounds
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func updateWishlist(_ sender: Any) {
        let wishlist = Techstore.getWishlist()
        
        DispatchQueue.main.async(execute: {
            self.produtosCollection.arrayData = wishlist
            self.produtosCollection.collectionView?.reloadData()
        })
    }
}
