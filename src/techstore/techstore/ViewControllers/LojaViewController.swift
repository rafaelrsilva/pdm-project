//
//  LojaViewController.swift
//  techstore
//
//  Created by Rafael on 25/09/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit
import MapKit

class LojaViewController: RRViewController {
    
    var lojaDetalhes: [String: AnyObject]!
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var lojaNome: UILabel!
    @IBOutlet weak var lojaEndereco: UILabel!
    @IBOutlet weak var lojaDescricao: UITextView!
    var lojaId: Int!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Techstore.getLoja(loja: self.lojaDetalhes["id"] as! Int, callback: {(loja: [String : AnyObject]?) -> Void in
            if loja != nil {
                self.lojaDetalhes = loja
                
                DispatchQueue.main.async(execute: {
                    self.lojaNome.text = self.lojaDetalhes["titulo"] as? String
                    self.lojaEndereco.text = self.lojaDetalhes["endereco"] as? String
                    self.lojaDescricao.text = self.lojaDetalhes["descricao"] as? String
                    
                    CLGeocoder().geocodeAddressString(self.lojaEndereco.text!, completionHandler: {(placemarks: [CLPlacemark]?, error:Error?) in
                        if error != nil {
                            print("Erro no geocoder: \(error)")
                        }
                        else{
                            if placemarks != nil && placemarks!.count > 0 {
                                let placemark = MKPlacemark(placemark: (placemarks!.first)!)
                                
                                //self.calcularDistancia()
                                
                                self.mapView.addAnnotation(placemark)
                                self.mapView.setRegion(MKCoordinateRegionMakeWithDistance(placemark.coordinate, 500, 500), animated: true)
                            }
                        }
                    })
                })
            }
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func fecharView(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
