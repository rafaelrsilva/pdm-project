//
//  LojasViewController.swift
//  techstore
//
//  Created by Rafael on 25/09/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class LojasViewController: RRViewController {

    @IBOutlet weak var container: UIView!
    var lojasTableView: LojasTableViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.lojasTableView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LojasTableViewController") as! LojasTableViewController
        self.lojasTableView.parentNavigationController = self.navigationController
        self.container.addSubview(self.lojasTableView.view)
        
        self.updateLojas(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func updateLojas(_ sender: Any) {
        Techstore.getLojas { (lojas: [[String : AnyObject]]? ) in
            DispatchQueue.main.async(execute: {
                self.lojasTableView.arrayData = lojas!
                self.lojasTableView.tableView.reloadData()
            })
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
