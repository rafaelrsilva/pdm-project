//
//  ProdutoCollectionViewCell.swift
//  techstore
//
//  Created by Rafael on 13/11/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class ProdutoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var produtoTitulo: UILabel!
    @IBOutlet weak var produtoImage: UIImageView!
    @IBOutlet weak var wishlist: UIImageView!
    var produtoDetalhes: [String: AnyObject]!
}
