//
//  LojaTableViewCell.swift
//  techstore
//
//  Created by Rafael on 14/11/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class LojaTableViewCell: UITableViewCell {
    
    var lojaDetalhes: [String: AnyObject]!
    
    @IBOutlet weak var lojaNome: UILabel!
    @IBOutlet weak var lojaEndereco: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
