//
//  ProdutosCollectionViewController.swift
//  techstore
//
//  Created by Rafael on 13/11/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ProdutoCollectionViewCell"

class ProdutosCollectionViewController: UICollectionViewController {
    
    var parentNavigationController: UINavigationController!
    var arrayData: [[String : AnyObject]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView!.backgroundColor = UIColor.clear
        self.collectionView!.backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayData.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ProdutoCollectionViewCell
        
        cell.produtoDetalhes = self.arrayData[indexPath.row]
        cell.produtoTitulo.text = cell.produtoDetalhes["titulo"] as? String
        
        if Techstore.isProdutoNaWishlist(id: cell.produtoDetalhes["id"] as! Int) != -1 {
            cell.wishlist.image = UIImage(named: "liked")
        }
        else{
            cell.wishlist.image = UIImage(named: "notLiked")
        }
        
        URLSession.shared.dataTask(with: URL(string: cell.produtoDetalhes["thumb"] as! String)!, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
            DispatchQueue.main.async(execute: {
                if error == nil && data != nil {
                    cell.produtoImage.image = UIImage(data: data!)
                }
                else{
                    cell.produtoImage.image = UIImage(named: "noImages")
                }
            })
        }).resume()

        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ProdutoCollectionViewCell
        let produtoVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProdutoViewController") as! ProdutoViewController
        
        produtoVC.produtoDetalhes = cell.produtoDetalhes
        
        self.parentNavigationController.pushViewController(produtoVC, animated: true)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
