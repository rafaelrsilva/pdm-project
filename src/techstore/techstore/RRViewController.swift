//
//  RRViewController.swift
//  techstore
//
//  Created by Rafael on 8/28/16.
//  Copyright © 2016 RR Desenvolvimento. All rights reserved.
//

import UIKit

class RRViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return UIStatusBarStyle.lightContent
        }
    }

}

