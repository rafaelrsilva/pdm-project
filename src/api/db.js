module.exports = (function(){
	var assets = 'http://rafaelrsilva.com:3000/img/';

	return {
		categorias: [
			{ id: 1, titulo: 'Desktops', background: 'http://exmple.com' },
			{ id: 2, titulo: 'Equipamentos de Rede', background: 'http://exmple.com' },
			{ id: 3, titulo: 'Impressoras', background: 'http://exmple.com' },
			{ id: 4, titulo: 'Notebooks', background: 'http://exmple.com' },
			{ id: 5, titulo: 'Smartphones', background: 'http://exmple.com' },
			{ id: 6, titulo: 'Tablets', background: 'http://exmple.com' },
		],
		produtos: [
			{ 
				id: 1,
				categoria: 1,
				titulo: 'Computador AIO LG BJ33P1', 
				descricao: 'Um notebook leve e versátil',
				thumb: assets + '01thumb.jpg',
				tabela: [
					{ chave: 'Processador', valor: 'Core i5 5200U' },
					{ chave: 'Memória', valor: '4 GB' },
					{ chave: 'HD', valor: '1 TB' },
					{ chave: 'Tamanho da Tela', valor: '23,8' },
					{ chave: 'Wireless', valor: '802.11 b/g/n' },
				],
				galeria: [
					{ src: assets + '01gal01.jpg' },
					{ src: assets + '01gal02.jpg' },
					{ src: assets + '01gal03.jpg' },
				]						
			},
			{ 
				id: 2,
				categoria: 1,
				titulo: 'All In One Samsung DP500A2L', 
				descricao: 'O All in One Samsung e a nova palavra em computadores. Sua solucao tudo em um, que combina computador com TV em uma tela de alta resolucao Full HD de 21.5 polegadas, oferece uma solucao completa para conteudos e multimidia. Seu design moderno e elegante harmoniza com todo tipo de ambiente, seja em casa ou no escritorio.',
				thumb: assets + '02thumb.jpg',
				tabela: [
					{ chave: 'Processador', valor: 'Core i3 6100U' },
					{ chave: 'Memória', valor: '4 GB' },
					{ chave: 'HD', valor: '500GB' },
					{ chave: 'Tamanho da Tela', valor: '21,5' },
					{ chave: 'Wireless', valor: '802.11 a/c' },
				],
				galeria: [
					{ src: assets + '02gal01.jpg' },
					{ src: assets + '02gal02.jpg' },
					{ src: assets + '02gal03.jpg' },
				]						
			},
			{ 
				id: 3,
				categoria: 1,
				titulo: 'iMac - MK142BZA', 
				descricao: 'Suporte a monitor externo e espelhamento de vídeo',
				thumb: assets + '03thumb.jpg',
				tabela: [
					{ chave: 'Processador', valor: 'Core i5 Dual Core' },
					{ chave: 'Memória', valor: '8 GB' },
					{ chave: 'HD', valor: '1 TB' },
					{ chave: 'Tamanho da Tela', valor: '21,5' },
					{ chave: 'Wireless', valor: '802.11 ac/abgn' },
				],
				galeria: [
					{ src: assets + '03gal01.jpg' },
					{ src: assets + '03gal02.jpg' },
				]						
			},
			{ 
				id: 4,
				categoria: 2,
				titulo: 'Roteador D-link Dual Band', 
				descricao: 'Inteligência, alta performance e supervelocidade para assistir a vídeos em HD, compartilhar fotos em altíssima resolução, jogar online e fazer download de músicas, tudo simultaneamente e muito mais rápido do que com os roteadores atuais',
				thumb: assets + '04thumb.jpg',
				tabela: [
					{ chave: 'Tipo Antenas', valor: 'Interna' },
					{ chave: 'Qtde Antenas', valor: '6' },
					{ chave: 'Gigabit', valor: 'Sim' },
					{ chave: 'Velocidade', valor: '1750 Mbps' },
				],
				galeria: [
					{ src: assets + '04gal01.jpg' },
					{ src: assets + '04gal02.jpg' },
				]						
			},
			{ 
				id: 5,
				categoria: 2,
				titulo: 'Roteador D-Link - DIR803', 
				descricao: 'Roteador Wireless Dual-Band com nova tecnologia Wi-Fi 11AC – 5ª geração Wi-Fi, a mais poderosa existente no mercado atualmente.',
				thumb: assets + '05thumb.jpg',
				tabela: [
					{ chave: 'Tipo Antenas', valor: 'Externa' },
					{ chave: 'Qtde Antenas', valor: '2' },
					{ chave: 'Frequencia', valor: '2,4 e 5 GHz' },
					{ chave: 'Velocidade', valor: '750 Mbps' },
					{ chave: 'Suporte a IPv6', valor: 'Sim' }
				],
				galeria: [
					{ src: assets + '05gal01.jpg' },
					{ src: assets + '05gal02.jpg' },
					{ src: assets + '05gal03.jpg' },
				]						
			},
			{ 
				id: 6,
				categoria: 2,
				titulo: 'Roteador AirPort Extreme', 
				descricao: 'Além de melhorar o desempenho do Wi-Fi, o novo design vertical diminui o espaço ocupado na mesa',
				thumb: assets + '06thumb.jpg',
				tabela: [
					{ chave: 'Cor', valor: 'Branco' },
					{ chave: 'Voltagem', valor: 'Bivolt' },
					{ chave: 'Garantia', valor: '12 meses' }
				],
				galeria: [
					{ src: assets + '06gal01.jpg' },
					{ src: assets + '06gal02.jpg' },
				]						
			},
			{ 
				id: 7,
				categoria: 2,
				titulo: 'Roteador e Repetidor D-Link', 
				descricao: 'Rede sem fios de altíssima velocidade para compartilhar a Internet com sua família, ouvir músicas, assistir filmes online e acessar redes sociais.',
				thumb: assets + '07thumb.jpg',
				tabela: [
					{ chave: 'Alcance', valor: 'Até 200m2' },
					{ chave: 'Tipo de Antena', valor: 'Externa' },
					{ chave: 'Qtde Antenas', valor: '3' },
					{ chave: 'IPv6', valor: 'Sim' },
					{ chave: 'Geração Wifi', valor: '802.11ac' },
				],
				galeria: [
					{ src: assets + '07gal01.jpg' },
					{ src: assets + '07gal02.jpg' },
					{ src: assets + '07gal03.jpg' },
				]						
			},
			{ 
				id: 8,
				categoria: 3,
				titulo: 'Impressora Multifuncional Epson', 
				descricao: 'Imprima ou digitalize diretamente de seu tablet ou smartphone',
				thumb: assets + '08thumb.jpg',
				tabela: [
					{ chave: 'Tipo', valor: 'Jato de Tinta' },
					{ chave: 'Conexões', valor: 'Wi-fi e USB' },
					{ chave: 'Vel. Impressão', valor: '26 ppm' },
					{ chave: 'Copiadora', valor: 'Sim' },
					{ chave: 'Scanner', valor: 'Sim' },
				],
				galeria: [
					{ src: assets + '08gal01.jpg' },
					{ src: assets + '08gal02.jpg' },
				]						
			},
			{ 
				id: 9,
				categoria: 3,
				titulo: 'Impressora Multifuncional HP', 
				descricao: 'Simplifique as tarefas com uma multifuncional acessível na qual você pode confiar.',
				thumb: assets + '09thumb.jpg',
				tabela: [
					{ chave: 'Tipo', valor: 'Jato de Tinta' },
					{ chave: 'Conexões', valor: 'USB' },
					{ chave: 'Ciclo Mensal', valor: '1000 páginas A4' },
					{ chave: 'Copiadora', valor: 'Sim' },
					{ chave: 'Scanner', valor: 'Sim' },
				],
				galeria: [
					{ src: assets + '09gal01.jpg' },
					{ src: assets + '09gal02.jpg' },
					{ src: assets + '09gal03.jpg' },
				]						
			},
			{ 
				id: 10,
				categoria: 3,
				titulo: 'Impressora Multifuncional Ecotank', 
				descricao: 'Imprima tudo o que quiser sem se preocupar se a tinta vai acabar.',
				thumb: assets + '10thumb.jpg',
				tabela: [
					{ chave: 'Tipo', valor: 'Jato de Tinta' },
					{ chave: 'Conexões', valor: 'Wi-fi e USB' },
					{ chave: 'Vel. Impressão', valor: '26 ppm' },
					{ chave: 'Copiadora', valor: 'Sim' },
					{ chave: 'Scanner', valor: 'Sim' },
				],
				galeria: [
					{ src: assets + '10gal01.jpg' },
					{ src: assets + '10gal02.jpg' },
					{ src: assets + '10gal03.jpg' },
				]						
			},
			{ 
				id: 11,
				categoria: 4,
				titulo: 'Notebook Samsung XE500C13-AD1BR', 
				descricao: 'Um notebook leve e versátil para quem vive conectado no agito do dia a dia e tem certeza que pode tudo menos perder tempo',
				thumb: assets + '11thumb.jpg',
				tabela: [
					{ chave: 'Processador', valor: 'Intel Celeron N3050' },
					{ chave: 'Memória', valor: '2 GB' },
					{ chave: 'HD', valor: '16 GB e.MMC iNAND' },
					{ chave: 'Tipo de Tela', valor: 'LED' },
					{ chave: 'Tamanho da Tela', valor: '11' },
					{ chave: 'Wireless', valor: '802.11 ac/abgn' },
				],
				galeria: [
					{ src: assets + '11gal01.jpg' },
					{ src: assets + '11gal02.jpg' },
					{ src: assets + '11gal03.jpg' },
					{ src: assets + '11gal04.jpg' },
				]						
			},
			{ 
				id: 12,
				categoria: 4,
				titulo: 'Notebook Asus X555UB-X250T', 
				descricao: 'Os novos notebooks da série X mantém o clássico e elegante design Zen ultrafino com a textura de círculos concêntricos. ',
				thumb: assets + '12thumb.jpg',
				tabela: [
					{ chave: 'Processador', valor: 'Core i5 6200U' },
					{ chave: 'Memória', valor: '8 GB' },
					{ chave: 'HD', valor: '1 TB' },
					{ chave: 'Tipo de Tela', valor: 'LED-blacklit' },
					{ chave: 'Tamanho da Tela', valor: '15,6' },
					{ chave: 'Wireless', valor: '802.11 b/g/n' },
				],
				galeria: [
					{ src: assets + '12gal01.jpg' },
					{ src: assets + '12gal02.jpg' },
					{ src: assets + '12gal03.jpg' },
					{ src: assets + '12gal04.jpg' },
				]						
			},
			{ 
				id: 13,
				categoria: 4,
				titulo: 'MacBook Pro 13 MD101BZ/A', 
				descricao: 'O MacBook Pro vem repleto de novos recursos que deixam esse notebook ainda melhor.',
				thumb: assets + '13thumb.jpg',
				tabela: [
					{ chave: 'Processador', valor: 'Core i5 Dual Core' },
					{ chave: 'Memória', valor: '4 GB' },
					{ chave: 'HD', valor: '500 GB' },
					{ chave: 'Tipo de Tela', valor: 'LED' },
					{ chave: 'Tamanho da Tela', valor: '13,3' },
					{ chave: 'Wireless', valor: '802.11 abgn' },
				],
				galeria: [
					{ src: assets + '13gal01.jpg' },
					{ src: assets + '13gal02.jpg' },
					{ src: assets + '13gal03.jpg' },
				]						
			},
			{ 
				id: 14,
				categoria: 4,
				titulo: 'Notebook Lenovo ideapad', 
				descricao: 'Mais agilidade em sua vida',
				thumb: assets + '14thumb.jpg',
				tabela: [
					{ chave: 'Processador', valor: 'Core i7 6500U' },
					{ chave: 'Memória', valor: '8 GB' },
					{ chave: 'HD', valor: '1 TB' },
					{ chave: 'Tamanho da Tela', valor: '15,6' },
					{ chave: 'Wireless', valor: '802.11 a/c' },
				],
				galeria: [
					{ src: assets + '14gal01.jpg' },
					{ src: assets + '14gal02.jpg' },
				]						
			},
			{ 
				id: 15,
				categoria: 4,
				titulo: 'Notebook Lenovo 2 em 1 ', 
				descricao: 'Gira 360 graus para você usar seu notebook de 4 maneiras diferentes.',
				thumb: assets + '15thumb.jpg',
				tabela: [
					{ chave: 'Processador', valor: 'Core i3 6100U' },
					{ chave: 'Memória', valor: '4 GB' },
					{ chave: 'HD', valor: '500GB' },
					{ chave: 'Tamanho da Tela', valor: '14' },
					{ chave: 'Wireless', valor: '802.11 a/c' },
				],
				galeria: [
					{ src: assets + '15gal01.jpg' },
					{ src: assets + '15gal02.jpg' },
					{ src: assets + '15gal03.jpg' },
				]						
			},
			{ 
				id: 16,
				categoria: 5,
				titulo: 'Samsung Galaxy S7 Edge', 
				descricao: 'Galaxy S7 Edge apresenta um acabamento premium em metal e vidro com uma câmera com foco ultrarrápido e excelente captura de fotos em ambientes de baixa luminosidade',
				thumb: assets + '16thumb.jpg',
				tabela: [
					{ chave: 'SO', valor: 'Android 6.0' },
					{ chave: 'Capacidade', valor: '32 GB' },
					{ chave: 'Camera', valor: '12 MP' },
					{ chave: 'Camera Frontal', valor: '5 MP' },
					{ chave: 'Tipo da Tela', valor: 'Super AMOLED' },
				],
				galeria: [
					{ src: assets + '16gal01.jpg' },
					{ src: assets + '16gal02.jpg' },
					{ src: assets + '16gal03.jpg' },
				]						
			},
			{ 
				id: 17,
				categoria: 5,
				titulo: 'iPhone SE Cinza Espacial', 
				descricao: 'Bem-vindo ao iPhone SE, o telefone de 4 polegadas mais poderoso já feito. Ele traz o design que todo mundo já adora, reinventado de dentro para fora',
				thumb: assets + '17thumb.jpg',
				tabela: [
					{ chave: 'SO', valor: 'iOS' },
					{ chave: 'Capacidade', valor: '32 GB' },
					{ chave: 'Camera', valor: '12 MP' },
					{ chave: 'Camera Frontal', valor: '5 MP' },
					{ chave: 'Tipo da Tela', valor: 'Retina HD' },
				],
				galeria: [
					{ src: assets + '17gal01.jpg' },
					{ src: assets + '17gal02.jpg' },
					{ src: assets + '17gal03.jpg' },
				]						
			},
			{ 
				id: 18,
				categoria: 5,
				titulo: 'iPhone 6s Plus Cinza Espacia', 
				descricao: 'Quando você começa a usar, percebe como ele é diferente de tudo o que você já experimentou.',
				thumb: assets + '18thumb.jpg',
				tabela: [
					{ chave: 'SO', valor: 'iOS' },
					{ chave: 'Capacidade', valor: '64 GB' },
					{ chave: 'Camera', valor: '12 MP' },
					{ chave: 'Camera Frontal', valor: '5 MP' },
					{ chave: 'Tipo da Tela', valor: 'Retina HD' },
				],
				galeria: [
					{ src: assets + '18gal01.jpg' },
					{ src: assets + '18gal02.jpg' },
					{ src: assets + '18gal03.jpg' },
					{ src: assets + '18gal04.jpg' },
				]						
			},
			{ 
				id: 19,
				categoria: 6,
				titulo: 'iPad Pro Space Gray', 
				descricao: 'Em um iPad tão grande, nenhum detalhe é pequeno demais. Para criar o iPad Pro, nós levamos cada característica do iPad a uma nova dimensão',
				thumb: assets + '19thumb.jpg',
				tabela: [
					{ chave: 'SO', valor: 'iOS' },
					{ chave: 'Capacidade', valor: '128 GB' },
					{ chave: 'Camera', valor: '8 MP' },
					{ chave: 'Camera Frontal', valor: '1.2 MP' },
					{ chave: 'Tipo da Tela', valor: 'Retina HD' },
				],
				galeria: [
					{ src: assets + '19gal01.jpg' },
					{ src: assets + '19gal02.jpg' },
					{ src: assets + '19gal03.jpg' },
				]						
			}
		],
		lojas: [
			{
				id: 1,
				titulo: 'Tech Store Aricanduva',
				descricao: 'Horário de funcionamento de Segunda a Sábado das 10h às 22h Aos Domingos das 14h às 20h',
				endereco: 'Av. Aricanduva, 5555 - Vila Matilde, São Paulo - SP, 03527-900'
			},
			{
				id: 2,
				titulo: 'Tech Store Iguatemi',
				descricao: 'Horário de funcionamento de Segunda a Sábado das 10h às 22h Aos Domingos das 14h às 20h',
				endereco: 'Av. Brg. Faria Lima, 2232 - Jardim Paulistano, São Paulo - SP, 01489-900'
			},
			{
				id: 3,
				titulo: 'Tech Store Bourbon',
				descricao: 'Horário de funcionamento de Segunda a Sábado das 10h às 22h Aos Domingos das 14h às 20h',
				endereco: 'R. Palestra Itália, 500 - Vila Pompeia, São Paulo - SP, 03178-200'
			},
			{
				id: 4,
				titulo: 'Tech Store Frei Caneca',
				descricao: 'Horário de funcionamento de Segunda a Sábado das 10h às 22h Aos Domingos das 14h às 20h',
				endereco: 'R. Frei Caneca, 569 - Consolação, São Paulo - SP, 01307-001'
			},
			{
				id: 5,
				titulo: 'Tech Store Cidade São Paulo',
				descricao: 'Horário de funcionamento de Segunda a Sábado das 10h às 22h Aos Domingos das 14h às 20h',
				endereco: 'Av. Paulista, 1230 - Bela Vista, São Paulo - SP, 01310-100'
			},
		]
	};
}());