var express    = require('express');
var bodyParser = require('body-parser');

var app        = express();
var server     = require('http').Server(app);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/assets'));

var db = require('./db.js');

var router = express.Router();

router.use(function(req, res, next){
	res.header('Access-Control-Allow-Origin', '*');
    next();
});

router.get('/categorias', function(req, res){
	res.json(db.categorias);
});

router.get('/categorias/:id/produtos', function(req, res){
	var categoria = req.params.id;
	var produtos = [];

	for(var p in db.produtos){
		var produto = db.produtos[p];

		if(produto.categoria == categoria){
			produtos.push({
				id     : produto.id,
				titulo : produto.titulo,
				thumb  : produto.thumb,
			});
		}
	}

  	res.json(produtos);
});

router.get('/produtos/:id', function(req, res) {
	var id_produto = req.params.id;

	for(var p in db.produtos){
		var produto = db.produtos[p];

		if(produto.id == id_produto){
			res.json(produto);
		}
	}
});

router.get('/lojas', function(req, res) {
	var lojas = [];

	for(var l in db.lojas){
		var loja = db.lojas[l];

		lojas.push({
			id       : loja.id,
			titulo   : loja.titulo,
			endereco : loja.endereco
		});
	}

	res.json(lojas);
});

router.get('/lojas/:id', function(req, res) {
	var id_loja = req.params.id;

	for(var l in db.lojas){
		var loja = db.lojas[l];

		if(loja.id == id_loja){
			res.json(loja);
		}
	}
});

app.use('/api', router);

server.listen(3000, function(){
    console.log('Tech Store API rodando na porta 3000');
});