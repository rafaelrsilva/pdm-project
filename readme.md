# Projeto de Programação em Dispositivos Móveis

## Descrição do Projeto

Desenvolvimento de app iOS, usando swift, para uma loja fictícia chamada **Tech Store**, especializada em vendas de produtos eletrônicos

## Conteúdo do App

* Lista de produtos por categoria ~~ + subcategoria~~
* Detalhes dos produtos (tabela de caracteristicas e galeria de fotos)
* Wishlist (produtos favoritados pelo usuário)
* Lista de unidades da loja + visualização no mapa (Usando MapKit)

## Conteúdo do Repositório:

* Wireframe
* Source Code (App + API)

## Observações

* Logo do projeto criados na plataforma [Logaster](https://www.logaster.com.br)
* ~~Todo o conteúdo do projeto é estático. Não há nenhuma integração externa~~
* Consumo de dados através de API Node